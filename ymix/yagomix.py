#!/usr/bin/python
try:
    import pygame
    from mutagen.mp3 import MP3
except:
    print("Hace falta pygame, tweepy y mutagen.")
    exit(4)
import datetime
import sys
from random import randint
from os import listdir, _exit  # , system # solo si se va a usar mpg123
from os.path import isfile, join
from time import sleep
from platform import platform

from ymix.libs.configs import *
from ymix.libs.funcs import *

IS_WINDOWS = "Windows" in platform()
if IS_WINDOWS:
    from multiprocessing import Process
else:
    from os import fork

cancionesBitrateRaro = ["El_venao.mp3"]
cancionesHardcoded = {
    "Call_me_maybe.mp3": 48000,
    "Ella_quiere_un_pedazo.mp3": 48000,
    "Danza_Kuduro.mp3": 48000}

now = datetime.datetime.now()
now = now.strftime("%m-%d_%H:%M:%S")


def playSong(song, by=""):
    if IS_WINDOWS:
        pid = 0
    else:
        pid = fork()
    if pid == 0:
        if not IS_WINDOWS:
            addToSignals()
        print("\033[95m\033[4m\033[5m Reproduciendo: " + song.split("/")[-1] + "\033[0m")
        aud = MP3(song)  # INFO del MP3
        if aud.info.sketchy:
            print("You biatch, that's not a MP3 file!")
            _exit(5)
        seconds = aud.info.length
        bitrate = aud.info.bitrate
        #print bitrate
        freq = 44100 * (bitrate/128000.0)

        if song.split("/")[-1] in cancionesBitrateRaro:
            pygame.mixer.init(frequency=int(freq))
        elif song.split("/")[-1] in cancionesHardcoded.keys():
            pygame.mixer.init(frequency=cancionesHardcoded[song.split("/")[-1]])
        else:
            pygame.mixer.init()

        pygame.mixer.music.set_volume(1.0)
        pygame.mixer.music.load(open(song, "rb"))
        clock = pygame.time.Clock()
        sleep (0.5)
        pygame.mixer.music.play()
        log("songs.log", "%s - %s\n" % (now, song.split("/")[-1]), "playSong")
        setPlaying(1) #Cerrojo mierder
        try:
            tweetSong(song.split("/")[-1], by)
        except Exception as e:
            log("twitter.log", str(e), "playSong")

        sleep(0.3)
        sleep(seconds)
        setPlaying(0)  # Cerrojo mierder
        _exit(0)  # _exit se tiene que usar en el hijo, si se usase exit el hijo seguiria corriendo
    else:
        pass

def play(song, by=""):
    if IS_WINDOWS:
        p = Process(target=playSong, args=(song, by))
        p.start()
    else:
        playSong(song, by)
    return 0


def checkAllSongsPlayed(filesPlayed, allSongs, path):
    if len(allSongs) == 0:
        writeSongs(path, [])
        return True
    return False


def getAllSongs(path, filesPlayed):
    #Coge los nombres de fichero del path en un array
    #SOLO los ficheros que NO empiecen por . terminen en .mp3 y no esten en filesReaded
    return [f for f in listdir(path)
            if isfile(join(path, f))
            and f[0] != "."
            and f[-4:] == ".mp3"
            and f not in filesPlayed]


def playThatAwesomeSong(song, by=""):
    songs = getAllSongs('/'.join(song.split("/")[:-1]), [])
    songSingle = song.split("/")[-1]
    listaPosibles = [s for s in songs if songSingle in parseSong(s)]
    if len(listaPosibles) == 0:
        print(song, " o derivado no esta en la lista de canciones.")
        return 1
    a = '/'.join(song.split("/")[:-1])
    song = a + "/" + listaPosibles[0]
    play(song, by)
    return 0


def playRandomSong(fileRead, path):
    filesPlayed = loadFilesPlayed(fileRead)
    allSongs = getAllSongs(path, filesPlayed)

    if checkAllSongsPlayed(filesPlayed, allSongs, fileRead):
        filesPlayed = []
        allSongs = getAllSongs(path, filesPlayed)

    songR = randint(0, len(allSongs)-1)
    song = allSongs[songR]
    filesPlayed.append(song)
    writeSongs(fileRead, filesPlayed)

    play(path+str(song))
    return 0


def main():
    init_config_file()
    argv = sys.argv
    res = 3
    path, fileRead, thisAwesomeSong,\
        maxRand, numToGetRand = getConfigurationValues()
    if thisAwesomeSong != "":  # Si se ha forzado una cancion, dale
        deleteSongConfig()
        res = playThatAwesomeSong(path + thisAwesomeSong)
        exit(res)
    if len(argv) >= 2 and (argv[1] == "-f" or argv[1] == "--force"):  # Force! =D
        if getPlaying():
            print("Reproduciendo ya.")
            exit(1)
        if len(argv) == 3:
            res = playThatAwesomeSong(path + argv[2])
        else:
            res = playRandomSong(fileRead, path)
        exit(res)
    numR = randint(1, maxRand)
    if numR != numToGetRand:
        log("songs.log", "%s(%s/%s) - %s\n" % (now, str(numR), str(numToGetRand), "Not this time!"), "yagomix.py")
        exit(2)
    if not getPlaying():
        res = playRandomSong(fileRead, path)
    exit(res)


if __name__ == "__main__":
    main()
