#!/usr/bin/python
try:
    import psutil
except Exception:
    print("Necesitas psutil: pip install psutil")
    exit(1)
from platform import platform

try:
    from ymix.libs.configs import setPlaying
except:
    from libs.configs import setPlaying    

IS_WINDOWS = "Windows" in platform()
VERSION = False

MAIN = []
CHANGE_CONF = True


def checkWindows(p, array):
    global CHANGE_CONF
    if len(array) > 4:
        if "multiprocessing" in array[3]:
            p.terminate()
            if CHANGE_CONF:
                #setPlaying(p.cwd()+"\\", 0)
                setPlaying(0)
                CHANGE_CONF = False


def checkLinux(p, array):
    global VERSION
    global CHANGE_CONF
    if len(array) >= 2:
        if "yagomix" in array[1]:
            if CHANGE_CONF:
                if VERSION:
                    #setPlaying(p.cwd()+"/", 0)
                    setPlaying(0)
                else:
                    #setPlaying(p.getcwd()+"/", 0)
                    setPlaying(0)
                CHANGE_CONF = False
            p.terminate()
        elif "daemonTweet" in array[1]:
            MAIN.append(p)


def checkTwitter():
    global CHANGE_CONF
    if len(MAIN) > 1:
        for p in MAIN[1:]:
            try:
                if CHANGE_CONF:
                    if VERSION:
                        setPlaying(0)
                    else:
                        setPlaying(0)
                    CHANGE_CONF = False
                p.terminate()
            except Exception:
                continue


def main():
    global VERSION
    pids = []
    try:
        pids = psutil.pids()
        VERSION = True
    except:
        pass
    if not VERSION:
        try:
            pids = psutil.get_pid_list()
            VERSION = False
        except:
            print("Error, psutil no compatible")
            exit(1)
    for i in pids:
        try:
            p = psutil.Process(i)
            if VERSION:
                array = p.cmdline()
            else:
                array = p.cmdline
            if "python" in array[0]:
                if IS_WINDOWS:
                    checkWindows(p, array)
                else:
                    checkLinux(p, array)
        except Exception:
            continue
    checkTwitter()


if __name__ == "__main__":
    main()
