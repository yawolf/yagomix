from ymix.yagomix import main as yagomix
from ymix.kill import main as kill2
from ymix.daemontweet import main as daemontweet
from time import sleep

def main():
	yagomix()

def kill():
	kill2()

def daemon():
	daemontweet()

if __name__ == "__main__":
	main()