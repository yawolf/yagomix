try:
	import tweepy
except:
    print("Hace falta tweepy.")
    exit(4)
import pickle
import datetime
from random import randint
from os import _exit, makedirs
from os.path import isdir
from signal import SIGABRT, SIGILL, SIGINT, SIGSEGV, SIGTERM, signal

try:
    from ymix.libs.configs import setPlaying, getAuth, PATH
except:

    from libs.configs import setPlaying, getAuth, PATH

def parseSong(song):
    song = song.replace("_", "")
    song = song.replace(".mp3", "")
    song = song.lower()
    return song


def loadFilesPlayed(path):
    read = []
    try:
        with open(path, 'rb') as f:
            read = pickle.load(f)
    except:
        return read
    return read


def writeSongs(path, lista):
    with open(path, 'wb') as f:
        pickle.dump(lista, f)


def addToSignals():
    for sig in (SIGABRT, SIGILL, SIGINT, SIGSEGV, SIGTERM):
        signal(sig, cleanFromSignal)


def cleanFromSignal(*args):
    setPlaying(0)
    _exit(6)
    exit(6)


def log(fileName, text, source):
    if not isdir(PATH):
        makedirs(PATH)
    fileName = PATH + "/" + fileName
    now = datetime.datetime.now()
    now = now.strftime("%m-%d_%H:%M:%S")
    if fileName.split("/")[-1] != "songs.log":
        text = "%s\n%s\n%s\n%s\n" % ("-"*30, "Log in " + source + " at " + now, "-"*30, text)
    with open(fileName, "a") as f:
        f.write(text)


def tweetSong(song, by=""):
    CARAS = ["=D", ":D", ":)", "=)", ":P", "=P", ":]", "=]", ":3", "=3", "<3"]

    # Consumer keys and access tokens, used for OAuth
    consumer_key, consumer_secret, access_token, access_token_secret = getAuth()

    song = song.replace("_", " ")
    song = song.split(".mp3")[0]

    # OAuth process, using the keys and tokens
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    # Creation of the actual interface, using authentication
    api = tweepy.API(auth)

    # Tweet the song being played
    if by != "":
        by = " by @" + by
    cara = CARAS[randint(1, len(CARAS)-1)]
    msg = "Now playing : " + song + by + " #YagoMixACM " + cara
    #print (msg) #DEBUG
    try:
        api.update_status(msg)
    except Exception as e:
        log("twitter.log", str(e), "tweetSong")


if __name__ == "__main__":
	print("Sorry, but this just can be imported and not executed")