from sys import version_info
if version_info.major == 3:
    import configparser as ConfigParser
else:
    import ConfigParser
from os import makedirs, getenv
from os.path import isfile, expanduser, isdir
from platform import platform

IS_WINDOWS = "Windows" in platform()
if IS_WINDOWS:
    PATH = expanduser("~") + "\\.yagomix"
    CONFIG = PATH + "\\yagomix.config"
    TWITTER = PATH + "\\twt.config" 
    SONGSREADED = PATH + "\\songsReaded"   
else:
    USER = getenv("USER")
    SUDO_USER = getenv("SUDO_USER")
    if not SUDO_USER:
        PATH = expanduser("~") + "/.yagomix"
    else:
        PATH = "/home/%s/.yagomix" % SUDO_USER
    CONFIG = PATH + "/yagomix.config"
    TWITTER = PATH + "/twt.config"
    SONGSREADED = PATH + "/songsReaded"


def init_config_file():
    global PATH
    global CONFIG
    global TWITTER
    global SONGSREADED
    global USER
    global SUDO_USER
    if USER=="root" and not SUDO_USER:
        print("You are executing from root, we don't want you to configure it in root")
        exit(1)
    if not isdir(PATH):
        makedirs(PATH)
    if not isfile(CONFIG):
        print("We've detected that you don't have a config file in " + CONFIG)
        print("So, we are going to generate one for you, but first, let me take a...")
        path=""
        while not isdir(path):
            if version_info.major == 3:
                path = input("No, seriously, tell me your Songs full path: ")
            else:
                path = raw_input("No, seriously, tell me your Songs full path: ")
        if path[-1] != "/" and not IS_WINDOWS:
            path += "/"
        elif path[-1] != "\\" and IS_WINDOWS:
            path += "\\"
        print("Creating...")
        with open(CONFIG, "w") as f:
            f.write("[yagomix]\n")
            f.write("pathsongs = %s\n" % path)
            f.write("fileread = %s\n" % SONGSREADED)
            f.write("maxrand = 6\nnumtogetrand = 3\nplaying = 0\nthisawesomesong = ")
        print("Created! =D You can modify when you want in %s" % CONFIG)  
    if not isfile(TWITTER):
        print("You don't have a twitter config, we'll create a basic one for you, but modify in " + TWITTER)
        print("Creating...")
        with open(TWITTER, "w") as f:
            f.write("[twitter]\nconsumer_key = <your consumer key>\nconsumer_secret = <your consumer secret>\
                \naccess_token = <your access token(app)>\naccess_token_secret = <your access token secret>")
        print("Created! =D You'll need to modify it in %s" % TWITTER)


def setPlaying(num):
    global CONFIG
    parser = ConfigParser.SafeConfigParser()
    parser.read(CONFIG)
    parser.set("yagomix", "playing", str(num))
    with open(CONFIG, "w") as f:
        parser.write(f)


def getPlaying():
    global CONFIG
    parser = ConfigParser.SafeConfigParser()
    parser.read(CONFIG)
    valor = parser.get("yagomix", "playing")
    return int(valor)


def deleteSongConfig():
    global CONFIG
    parser = ConfigParser.SafeConfigParser()
    parser.read(CONFIG)
    parser.set("yagomix", "thisAwesomeSong", "")
    with open(path, "w") as f:
        parser.write(f)


def getAuth():
    global TWITTER
    init_config_file()
    parser = ConfigParser.SafeConfigParser()
    parser.read(TWITTER)
    cKey = parser.get("twitter", "consumer_key")
    cSecret = parser.get("twitter", "consumer_secret")
    acc_token = parser.get("twitter", "access_token")
    acc_token_sec = parser.get("twitter", "access_token_secret")
    return cKey, cSecret, acc_token, acc_token_sec


def getConfigurationValues():
    global CONFIG
    init_config_file()
    parser = ConfigParser.SafeConfigParser(allow_no_value=True)
    parser.read(CONFIG)
    pathSongs = parser.get("yagomix", "pathSongs") # dirname(abspath(__file__)) + "/" + parser.get("yagomix", "pathSongs")
    fileRead = parser.get("yagomix", "fileRead")
    maxRand = int(parser.get("yagomix", "maxRand"))
    numToGetRand = int(parser.get("yagomix", "numToGetRand"))
    if parser.has_option("yagomix", "thisAwesomeSong"):
        thisAwesomeSong = parser.get("yagomix", "thisAwesomeSong")
    else:
        thisAwesomeSong = ""

    return pathSongs, fileRead, thisAwesomeSong, maxRand, numToGetRand


if __name__ == "__main__":
	print("Sorry, but this just can be imported and not executed")