#! /usr/bin/python2

import tweepy
import os
from os.path import dirname, abspath, expanduser
from time import sleep
try:
    from ymix.yagomix import parseSong, playThatAwesomeSong, getPlaying, log, playRandomSong
    from ymix.libs.configs import getAuth, init_config_file, getConfigurationValues
except:
    from yagomix import parseSong, playThatAwesomeSong, getPlaying, log, playRandomSong
    from libs.configs import getAuth, init_config_file, getConfigurationValues
from platform import platform

IS_WINDOWS = "Windows" in platform()
if IS_WINDOWS:
    PATH_LAST_TWEET = expanduser("~") + "\\.yagomix\\.id_last_tweet" 
else:
    PATH_LAST_TWEET = expanduser("~") + "/.yagomix/.id_last_tweet"

"""
Ejemplos:
#yagomixacmdo play letitgo
#yagomixacmdo config maxrand 10  #In Development
"""

cKey, cSecret, acc_token, acc_token_sec = getAuth()

USERS_ADMIT = ["binaryrock", "ygwolf"]
ACTIONS_ADMIT = ["play", "songs"]
PLAYING = False
CARAS = ["=D", ":D", ":)", "=)", ":P", "=P", ":]", "=]", ":3", "=3", "<3"]
INDEX = 0

#OAUTH
auth = tweepy.OAuthHandler(cKey, cSecret)
auth.set_access_token(acc_token, acc_token_sec)

api = tweepy.API(auth)


def playSong(text, status):
    global PLAYING
    if len(text) > 0:
        if getPlaying():
            tweetAlreadyPlaying(status, api)
            return
        PLAYING = True
        song = parseSong(text.pop(0))
        configurations = getConfigurationValues()
        path = configurations[0]
        res = playThatAwesomeSong(path + song, status.user.screen_name)
        if res:
            tweetNotSong(song, status.user.screen_name)
    else:
        if getPlaying():
            tweetAlreadyPlaying(status, api)
            return
        PLAYING = True
        configurations = getConfigurationValues()
        path = configurations[0]
        fileRead = configurations[1]
        playRandomSong(fileRead,path)
        


def parseTweet(status, api):
    text = status.text                  # yagomixacmdo play letitgo
    text = text.replace("#yagomixacmdo", "")    # play letitgo
    text = text.split()                 # ["play","letitgo"]
    if len(text) > 0:
        action = text.pop(0)            # ["letitgo"]
        if action in ACTIONS_ADMIT:
            if action == "play":
                playSong(text, status)
            elif action == "songs":
                tweetSongs(status.user.screen_name, api)


def tweetSongs(user, api):
    global CARAS
    global INDEX
    msg = "Aqui tienes mis canciones amo! @%s %s %s" % (user, "http://pic.twitter.com/3pwNNigk3o", CARAS[INDEX])
    INDEX = (INDEX + 1) % (len(CARAS))
    try:
        api.update_status(msg)
    except Exception as e:
        log(".twitter.log", str(e), "daemonTweet")


def tweetNotSong(song, by=""):
    global CARAS
    global INDEX
    if by != "":
        by = " @%s " % by
    msg = "Lo siento " + by + " tu cancion " + song + " no exite " + CARAS[INDEX]
    INDEX = (INDEX + 1) % (len(CARAS))
    try:
        api.update_status(msg)
    except Exception as e:
        log(".twitter.log", str(e), "daemonTweet")


def tweetNoElit(api, status):
    global CARAS
    global INDEX
    msg = "Lo siento @" + status.user.screen_name + ", no estas en la elite " + CARAS[INDEX]
    INDEX = (INDEX + 1) % (len(CARAS))
    try:
        api.update_status(msg)
    except Exception as e:
        log(".twitter.log", str(e), "daemonTweet")


def tweetAlreadyPlaying(tweet, api):
    global CARAS
    global INDEX
    msg = "Lo siento @" + tweet.user.screen_name + ", ya estoy funcionando " + CARAS[INDEX]
    INDEX = (INDEX + 1) % (len(CARAS))
    try:
        api.update_status(msg)
    except Exception as e:
        log(".twitter.log", str(e), "daemonTweet")


def callateRamera(name, api):
    msg = ". @%s calla, ramera." % name
    try:
        api.update_status(msg)
    except Exception as e:
        log(".twitter.log", str(e), "daemonTweet")

def main():
    global PLAYING
    init_config_file()
    if not os.path.exists(PATH_LAST_TWEET):
        last_id = "434366517908897792"
    else:
        with open(PATH_LAST_TWEET, "r") as f:
            last_id = f.readline().strip()

    while True:
        tweets = api.search(q=["#yagomixacmdo"], since_id=last_id)
        for i in tweets:
            status = i
            if status.user.screen_name.lower() == "kami_lex":
                callateRamera("kami_lex", api)
            if PLAYING:
                tweetAlreadyPlaying(status, api)
                continue
            if status.user.screen_name.lower() in USERS_ADMIT:
                parseTweet(i, api)
            else:
                tweetNoElit(api, status)
            #Actualiza ID
            last_id = str(status.id)
            with open(PATH_LAST_TWEET, "w") as f:
                f.write(last_id)

        if PLAYING:
            sleep(300)
            PLAYING = not PLAYING
        else:
            sleep(60)

if __name__ == "__main__":
    main()
