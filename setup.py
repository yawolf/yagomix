from setuptools import setup, find_packages
import sys

sys.path.append('ymix')
import ymix


setup(name='yagomix',
      packages=find_packages(exclude=["__pycache__", "bootstart.py"]),
      version='1.0',
      author='Rock Neurotiko, Yago',
      author_email='rockneurotiko@gmail.com',
      url='http://bitbucket.com/yawolf/yagomix',
      download_url='https://bitbucket.org/yawolf/yagomix/downloads',
      description='A powerfull tool to play music randomly =D',
      #provides=['yagomix'],
      keywords='yagomix music mp3 random',
      license='Beerwaare License',
      classifiers=['Development Status :: 4 - Beta',
                   'Intended Audience :: Other Audience',
                   'Natural Language :: Spanish',
                   'Natural Language :: English',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python :: 2',
                   'Programming Language :: Python :: 3',
                   'License :: Freeware',
                   'Topic :: Games/Entertainment',
                   'Topic :: Multimedia :: Sound/Audio :: Players',
                   'Topic :: Multimedia :: Sound/Audio :: Players :: MP3',
                   'Topic :: Utilities',
                  ],
      entry_points={
        "console_scripts": [
            "yagomix=ymix:main",
            "killymix=ymix:kill",
            "daemonymix=ymix:daemon",
        ],
    },
     )
