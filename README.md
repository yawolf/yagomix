**YAGOMIX**
---


This is a free-software project made by some FI (ETSIINF) - UPM students and it's under the Beerware license (see LICENSE file)
-

Yagomix is suposed to be in some computer (we have it in a raspberry pi) connected to speakers (if have subwoofer better!)

**How we have it installed and configured?**

- raspberry with an 8GB SD (raspbian), ethernet and the speakers connected
- yagomix cloned in ~ (or whatever)
- a symlinc in ~/ymix to the project
- 1 cron every 5 minutes:
	
	`*/5 * * * * pi /home/pi/start.sh`

- 1 cron at start:
	
	`@reboot		pi /home/pi/atBoot.sh`

- all the dependecies installed (tweepy, mutagen and pygame to main, psutil to kill)

- yagomix.config in ~/.yagomix/ with this fields:

```
[yagomix]
pathsongs = <your path>
fileread = songsReaded
maxrand = 6
numtogetrand = 3
playing = 0
thisawesomesong = 
```

- twt.config with our twitter keys in ~/.yagomix/, with this syntax:
	
```
[twitter]
consumer_key = <your consumer key>
consumer_secret = <your consumer secret>
access_token = <your access token(app)>
access_token_secret = <your access token secret>
```
- That's all you need folks! =D
 
